import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('haproxy')


def test_haproxy_connection(host):
    assert host.socket("tcp://127.0.0.1:80").is_listening


def test_haproxy_http(host):

    request = host.run("curl localhost/test")

    assert 'URI: /test' in request.stdout
