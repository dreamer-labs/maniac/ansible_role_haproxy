import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('haproxy')


def test_haproxy_http_node_failure(host):

    request = host.run("curl localhost/test")

    assert 'Server name: node1' in request.stdout
