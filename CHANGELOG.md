# [2.0.0](https://gitlab.com/dreamer-labs/maniac/ansible_role_haproxy/compare/v1.0.0...v2.0.0) (2021-01-22)


### Bug Fixes

* bring role up to current standards ([e1d69af](https://gitlab.com/dreamer-labs/maniac/ansible_role_haproxy/commit/e1d69af))


### Features

* modify haproxy_services syntax ([7599cd0](https://gitlab.com/dreamer-labs/maniac/ansible_role_haproxy/commit/7599cd0))


### BREAKING CHANGES

* modifies variables to make them more clear

Previously we were relying on Ansible to determine IP addresses
and hostnames for the backend nodes. Now we specify those
explicitly.

# 1.0.0 (2020-02-20)


### Features

* Add the ability to turn on stats interface ([ba17bfa](https://gitlab.com/dreamer-labs/maniac/ansible_role_haproxy/commit/ba17bfa)), closes [#5](https://gitlab.com/dreamer-labs/maniac/ansible_role_haproxy/issues/5)
